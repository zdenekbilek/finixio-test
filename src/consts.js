export const COINS = ['BTC', 'ETH', 'BNB', 'XRP', 'USDT', 'ADA', 'DOT', 'UNI', 'LTC', 'LINK'];

export const CURRENCIES = ['USD', 'EUR', 'JPY', 'GBP', 'AUD', 'CAD', 'CHF', 'CNY', 'HKD', 'NZD'];

export const MAIN_CURRENCY = 'USD';

export const API = 'https://min-api.cryptocompare.com';
