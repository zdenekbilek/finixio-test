import React from 'react';
import PropTypes from 'prop-types';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import { CURRENCIES } from '../consts';

const cells = [
    {
        id: 'name',
        getLabel: () => `Coin Name`,
    },
    {
        id: 'current_price',
        getLabel: ({ currency }) => `Current Price (${currency})`,
    },
    {
        id: 'opening_price',
        getLabel: ({ currency }) => `Opening Price (${currency})`,
    },
    {
        id: 'price_increase',
        getLabel: () => 'Price Increase'
    }
];

function CryptoTableHead({ onSortRequest, orderBy, order, currency }) {
    return (
        <TableHead>
            <TableRow>
                {cells.map(({ id, getLabel }) => (
                    <TableCell key={id}>
                        <TableSortLabel
                            active={orderBy === id}
                            direction={orderBy === id ? order : 'asc'}
                            onClick={(e) => onSortRequest(e, id)}
                        >
                            {getLabel({ currency })}
                        </TableSortLabel>
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    )
}

CryptoTableHead.propTypes = {
    onSortRequest: PropTypes.func.isRequired,
    orderBy: PropTypes.string.isRequired,
    order: PropTypes.oneOf(['asc', 'desc']).isRequired,
    currency: PropTypes.oneOf(CURRENCIES),
};

export default CryptoTableHead;