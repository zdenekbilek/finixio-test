import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { CURRENCIES } from '../consts';

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    }
}));

function CurrencySelector({ value, onChange }) {
    const classes = useStyles();

    return (
        <FormControl className={classes.formControl}>
            <InputLabel id="currency-selector">Currency</InputLabel>
            <Select labelId="currency-selector" value={value} onChange={onChange}>
                {CURRENCIES.map(currency => (
                    <MenuItem key={currency} value={currency}>{currency}</MenuItem>
                ))}
            </Select>
        </FormControl>
    );
}

CurrencySelector.propTypes = {
    value: PropTypes.oneOf(CURRENCIES).isRequired,
    onChange: PropTypes.func.isRequired,
};

export default CurrencySelector;