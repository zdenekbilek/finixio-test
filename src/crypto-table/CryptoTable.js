import React, { useState } from 'react';
import Table from '@material-ui/core/Table';
import useFetchPriceList from './useFetchPriceList';
import { MAIN_CURRENCY } from '../consts';
import CurrencySelector from './CurrencySelector';
import CryptoTableHead from './CryptoTableHead';
import CryptoTableBody from './CryptoTableBody';

function CryptoTable() {
    const [currency, setCurrency] = useState(MAIN_CURRENCY);
    const [data] = useFetchPriceList(currency);
    const [order, setOrder] = useState('desc');
    const [orderBy, setOrderBy] = useState('price_increase');

    const onSortRequest = (e, key) => {
        const isAsc = orderBy === key && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(key);
    };

    const handleCurrencyOnChange = e => {
        setCurrency(e.target.value);
    };

    return (
        <React.Fragment>
            <CurrencySelector value={currency} onChange={handleCurrencyOnChange} />
            <Table>
                <CryptoTableHead
                    currency={currency}
                    order={order}
                    orderBy={orderBy}
                    onSortRequest={onSortRequest}
                />
                <CryptoTableBody data={data} orderBy={orderBy} order={order} currency={currency} />
            </Table>
        </React.Fragment>
    );
}

export default CryptoTable;