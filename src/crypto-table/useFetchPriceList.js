import { useState, useEffect } from 'react';
import { API, COINS } from '../consts';
import { getPercentageDifference } from './helpers';

function transformPriceList(response, currency) {
    if (!response.RAW) {
        throw new Error('Invalid Data');
    }

    return Object.values(response.RAW).map(obj => {
        const { FROMSYMBOL, OPENDAY, PRICE } = obj[currency];

        return {
            name: FROMSYMBOL,
            price: PRICE,
            opening_price: OPENDAY,
            price_increase: getPercentageDifference(OPENDAY, PRICE),
        };
    });
};

export default function useFetchPriceList(currency) {
    const [data, setData] = useState(null);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        const fetchData = async () => {
            try {
                setLoading(true);
                const response = await fetch(`${API}/data/pricemultifull?fsyms=${COINS.join(',')}&tsyms=${currency}`);
                const json = await response.json();

                setData(transformPriceList(json, currency));
            } catch (e) {
                console.log(e, 'error');
            } finally {
                setLoading(false);
            }
        };

        fetchData();
    }, [currency]);

    return [data, loading];
}