import getSymbolFromCurrency from 'currency-symbol-map';

export function formatMoney(number, currency) {
    const symbol = getSymbolFromCurrency(currency) || '';

    return `${symbol}${number.toLocaleString()}`;
}

export function getPercentageDifference(oldNum, newNum) {
    const difference = oldNum - newNum;

    return (difference / oldNum) * 100;
}