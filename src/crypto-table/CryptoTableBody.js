import React from 'react';
import PropTypes from 'prop-types';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import lodashOrderBy from 'lodash.orderby';
import { formatMoney } from './helpers';
import { CURRENCIES } from '../consts';

function CryptoTableBody({ data, orderBy, order, currency }) {
    return (
        <TableBody>
            {data && lodashOrderBy(data, orderBy, order).map(({ name, price, opening_price, price_increase }) => (
                <TableRow key={name}>
                    <TableCell>{name}</TableCell>
                    <TableCell>{formatMoney(price.toLocaleString(), currency)}</TableCell>
                    <TableCell>{formatMoney(opening_price.toLocaleString(), currency)}</TableCell>
                    <TableCell>{price_increase.toLocaleString()}% ({formatMoney(opening_price - price, currency)})</TableCell>
                </TableRow>
            ))}
        </TableBody>
    );
}

CryptoTableBody.propTypes = {
    data: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        opening_price: PropTypes.number.isRequired,
        price_increase: PropTypes.number.isRequired,
    })),
    orderBy: PropTypes.string.isRequired,
    order: PropTypes.oneOf(['asc', 'desc']).isRequired,
    currency: PropTypes.oneOf(CURRENCIES),
};

export default CryptoTableBody;