import './App.css';
import CryptoTable from './crypto-table/CryptoTable';

function App() {
  return (
    <CryptoTable />
  );
}

export default App;
